Deface::Override.new(:virtual_path => "spree/admin/orders/index",
                     :name => "add_delivery_date_table_header",
                     :insert_after => "[data-hook='admin_orders_index_headers'] th:first",
                     :partial => "spree/admin/orders/delivery_date_header",
                     :disabled => false)
Deface::Override.new(:virtual_path => "spree/admin/orders/index",
                     :name => "add_delivery_date_table_row_cell",
                     :insert_after => "[data-hook='admin_orders_index_rows'] td:first",
                     :partial => "spree/admin/orders/delivery_date_cell",
                     :disabled => false)