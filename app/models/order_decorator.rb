Spree::Order.class_eval do
  require 'date'
  require 'spree/order/checkout'

  attr_accessible :delivery_date

  validate :delivery_date, :presence => true, :allow_nil => false
  validate :delivery_date_specific_validation

  # Ensure that a delivery date is set. We don't want to run these validations until it is
  # Only run the delivery date validations if we are on that step or past.
  def delivery_date_specific_validation

    if !delivery_date.blank? && ['payment', 'confirm', 'complete'].include?(state)
      #cutoff = Time.zone.now.change(:hour => 16, :min => 00) # Gets 4pm in EST time zone (config.time_zone)


      #if [0, 1, 7].include?(delivery_date.wday)
      #  errors.add(:delivery_date, "cannot be a Sunday or Monday.")
      #end

      #if cutoff.past? && !(delivery_date > Date.tomorrow)
      #  errors.add(:delivery_date, ": It is too late for delivery tomorrow. Please specify a date after tomorrow.")
      #elsif !cutoff.past? && !(delivery_date > Date.today)
      #  errors.add(:delivery_date, ": It is too late for delivery today. Please specify a date tomorrow or later.")
      #end

      if delivery_date < Date.today.advance(:days => days_to_be_ready)
        errors.add(:base, "No podemos asegurar la entrega en la fecha elegida. Por favor, elige otra fecha")
      end
    end
  end

  def days_to_be_ready
    days = products.inject(0) { |days_to_be_ready, p| [days_to_be_ready, p.days_to_be_ready].max }
    weekend_days = (Date.today..Date.today.advance(:days => days)).select { |d| [0, 6, 7].include?(d.wday) }.count  
    days_to_be_ready = days + weekend_days
  end
end