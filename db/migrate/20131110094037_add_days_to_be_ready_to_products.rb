class AddDaysToBeReadyToProducts < ActiveRecord::Migration
  def up
    add_column :spree_products, :days_to_be_ready, :integer, :default => 0
  end

  def down
    remove_column :spree_products, :days_to_be_ready
  end
end
